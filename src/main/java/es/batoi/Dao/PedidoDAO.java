/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicPedido;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class PedidoDAO implements GenericDAO {

    private final Session sesion;

    public PedidoDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicPedido findByPK(int id) throws Exception {
        return (BatoiLogicPedido) sesion.get(BatoiLogicPedido.class, id);
    }

    public BatoiLogicPedido findByPKAdvance(int parseInt, int id) throws Exception {
        List l = sesion.createQuery("select a from BatoiLogicPedido a where a.batoiLogicCliente.id = " + id + " and a.id = " + parseInt).list();
        return (BatoiLogicPedido) l.get(0);
    }

    @Override
    public List<BatoiLogicPedido> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicPedido a").list();
    }

    public List<BatoiLogicPedido> findAllId(int id) throws Exception {
        return sesion.createQuery("select a from BatoiLogicPedido a where a.batoiLogicCliente.id = " + id).list();
    }

    @Override
    public List<BatoiLogicPedido> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {

        return sesion.createQuery(sqlselect).list();

    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicPedido pedInsertar = (BatoiLogicPedido) t;
        sesion.beginTransaction();
        sesion.save(pedInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        // Articulo artActualizar = (Articulo) t;
        sesion.getTransaction().begin();
        // sesion.update(grupoActualizar);   // no necesario
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicPedido pedBorrar = findByPK(id);
        delete(pedBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicPedido pedBorrar = (BatoiLogicPedido) t;
        sesion.getTransaction().begin();
        sesion.delete(pedBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    public void cerrar() {
        sesion.close();
    }

}
