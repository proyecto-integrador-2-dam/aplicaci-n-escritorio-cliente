/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicCp;
import es.batoi.modelos.BatoiLogicDireccion;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class DireccionDAO implements GenericDAO {

    private final Session sesion;

    public DireccionDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicDireccion findByPK(int id) throws Exception {
        return (BatoiLogicDireccion) sesion.get(BatoiLogicDireccion.class, id);
    }

    @Override
    public List<BatoiLogicDireccion> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicDireccion a").list();
    }

    public List<BatoiLogicDireccion> findAllId(int id) throws Exception {
        return sesion.createQuery("select a from BatoiLogicDireccion a where a.batoiLogicCliente.id = " + id).list();
    }

    @Override
    public List<BatoiLogicDireccion> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicDireccion cliInsertar = (BatoiLogicDireccion) t;
        sesion.beginTransaction();
        sesion.save(cliInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        BatoiLogicDireccion cliUpdate = (BatoiLogicDireccion) t;
        BatoiLogicDireccion aux = (BatoiLogicDireccion) findByPK(cliUpdate.getId());
        sesion.getTransaction().begin();
        aux.setBatoiLogicCp(cliUpdate.getBatoiLogicCp());
        aux.setCalle(cliUpdate.getCalle());
        aux.getBatoiLogicCp().getBatoiLogicCiudad().setNombre(cliUpdate.getBatoiLogicCp().getBatoiLogicCiudad().getNombre());
        aux.getBatoiLogicCp().getBatoiLogicCiudad().setProvincia(cliUpdate.getBatoiLogicCp().getBatoiLogicCiudad().getProvincia());// no necesario
        aux.getBatoiLogicCp().getBatoiLogicCiudad().setPais(cliUpdate.getBatoiLogicCp().getBatoiLogicCiudad().getPais());// no necesario

        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicDireccion cliBorrar = findByPK(id);
        delete(cliBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicDireccion cliBorrar = (BatoiLogicDireccion) t;
        sesion.getTransaction().begin();
        sesion.delete(cliBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    // Ejemplo consulta: obtener sólo los nombres de los Clientes
    public List<String> findAllName() throws Exception {
        List<String> registros = sesion.createQuery("select nombre from BatoiLogicDireccion").list();
        return registros;
    }

    public void cerrar() {
        sesion.close();
    }

}
