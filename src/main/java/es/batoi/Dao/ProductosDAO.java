/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicProducto;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class ProductosDAO implements GenericDAO{
    private final Session sesion;

    public ProductosDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicProducto findByPK(int id) throws Exception {
        return (BatoiLogicProducto) sesion.get(BatoiLogicProducto.class, id);
    }

    @Override
    public List<BatoiLogicProducto> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicProducto a").list();
    }

    @Override
    public List<BatoiLogicProducto> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicProducto cliInsertar = (BatoiLogicProducto) t;
        sesion.beginTransaction();
        sesion.save(cliInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        // Articulo artActualizar = (Articulo) t;
        sesion.getTransaction().begin();
        // sesion.update(grupoActualizar);   // no necesario
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicProducto cliBorrar = findByPK(id);
        delete(cliBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicProducto cliBorrar = (BatoiLogicProducto) t;
        sesion.getTransaction().begin();
        sesion.delete(cliBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    // Ejemplo consulta: obtener sólo los nombres de los Clientes
    public List<String> findAllName() throws Exception  {
        List<String> registros = sesion.createQuery("select nombre from BatoiLogicProducto").list();
        return registros;
    }

    public void cerrar() {
        sesion.close();
    }

}
