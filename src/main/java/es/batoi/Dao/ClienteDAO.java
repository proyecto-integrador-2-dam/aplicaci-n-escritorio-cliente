/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicCliente;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class ClienteDAO implements GenericDAO {

    private final Session sesion;

    public ClienteDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicCliente findByPK(int id) throws Exception {
        return (BatoiLogicCliente) sesion.get(BatoiLogicCliente.class, id);
    }

    @Override
    public List<BatoiLogicCliente> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicCliente a").list();
    }

    @Override
    public List<BatoiLogicCliente> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicCliente cliInsertar = (BatoiLogicCliente) t;
        sesion.beginTransaction();
        sesion.save(cliInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        BatoiLogicCliente cliUpdate = (BatoiLogicCliente) t;
        BatoiLogicCliente aux = (BatoiLogicCliente) findByPK(cliUpdate.getId());

        sesion.getTransaction().begin();

        aux.setNombre(cliUpdate.getNombre());
        aux.setDni(cliUpdate.getDni());
        aux.setApellidos(cliUpdate.getApellidos());
        aux.setEmail(cliUpdate.getEmail());
        aux.setPassw(cliUpdate.getPassw());
        sesion.getTransaction().commit();   
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicCliente cliBorrar = findByPK(id);
        delete(cliBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicCliente cliBorrar = (BatoiLogicCliente) t;
        sesion.getTransaction().begin();
        sesion.delete(cliBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    // Ejemplo consulta: obtener sólo los nombres de los Clientes
    public List<String> findAllName() throws Exception {
        List<String> registros = sesion.createQuery("select nombre from BatoiLogicCliente").list();
        return registros;
    }

    public void cerrar() {
        sesion.close();
    }

}
