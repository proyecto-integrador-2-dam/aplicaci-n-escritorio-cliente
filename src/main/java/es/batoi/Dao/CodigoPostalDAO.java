/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicCp;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class CodigoPostalDAO implements GenericDAO {

    private final Session sesion;

    public CodigoPostalDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicCp findByPK(int id) throws Exception {
        return (BatoiLogicCp) sesion.get(BatoiLogicCp.class, id);
    }

    @Override
    public List<BatoiLogicCp> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicCp a").list();
    }

    public List<BatoiLogicCp> findAllCp() {
        return sesion.createQuery("select a.cp from BatoiLogicCp a").list();
    }

    @Override
    public List<BatoiLogicCp> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicCp cliInsertar = (BatoiLogicCp) t;
        sesion.beginTransaction();
        sesion.save(cliInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        BatoiLogicCp cliUpdate = (BatoiLogicCp) t;
        BatoiLogicCp aux = (BatoiLogicCp) findByPK(cliUpdate.getId());

        sesion.getTransaction().begin();
        aux.setCp(cliUpdate.getCp());
        
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicCp cliBorrar = findByPK(id);
        delete(cliBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicCp cliBorrar = (BatoiLogicCp) t;
        sesion.getTransaction().begin();
        sesion.delete(cliBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    public void cerrar() {
        sesion.close();
    }

}
