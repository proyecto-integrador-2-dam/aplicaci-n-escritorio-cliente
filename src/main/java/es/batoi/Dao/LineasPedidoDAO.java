/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicLinpedido;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class LineasPedidoDAO implements GenericDAO {

    private final Session sesion;

    public LineasPedidoDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicLinpedido findByPK(int id) throws Exception {
        return (BatoiLogicLinpedido) sesion.get(BatoiLogicLinpedido.class, id);
    }

    @Override
    public List<BatoiLogicLinpedido> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicLinpedido a").list();
    }

    public List<BatoiLogicLinpedido> findAllForPedido(int id) {
        return sesion.createQuery("select a from BatoiLogicLinpedido a where a.batoiLogicPedidoByPedido.id = "+id).list();
    }

    @Override
    public List<BatoiLogicLinpedido> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        /*System.out.println("No implementado");
        return null;*/
        return sesion.createQuery(sqlselect).list();
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicLinpedido pedInsertar = (BatoiLogicLinpedido) t;
        sesion.beginTransaction();
        sesion.save(pedInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        // Articulo artActualizar = (Articulo) t;
        sesion.getTransaction().begin();
        // sesion.update(grupoActualizar);   // no necesario
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicLinpedido pedBorrar = findByPK(id);
        delete(pedBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicLinpedido pedBorrar = (BatoiLogicLinpedido) t;
        sesion.getTransaction().begin();
        sesion.delete(pedBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    public void cerrar() {
        sesion.close();
    }

}
