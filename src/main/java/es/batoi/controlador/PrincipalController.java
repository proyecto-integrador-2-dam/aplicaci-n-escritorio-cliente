package es.batoi.controlador;

import es.batoi.App;
import es.batoi.Dao.*;
import es.batoi.modelos.*;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrincipalController implements Initializable {

    /**
     * GENERAL.
     */
    @FXML
    private AnchorPane rootPane;
    @FXML
    private Label lbUsername;

    private PedidoDAO pedidoDAO;
    private ClienteDAO clienteDAO;
    private ProductosDAO productosDAO;
    private LineasPedidoDAO lineasPedidoDAO;
    private DireccionDAO direccionDAO;

    private DecimalFormat df = new DecimalFormat("#.00");

    private static BatoiLogicCliente clienteLogueado;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        pedidoDAO = new PedidoDAO();
        productosDAO = new ProductosDAO();
        lineasPedidoDAO = new LineasPedidoDAO();
        direccionDAO = new DireccionDAO();
        clienteDAO = new ClienteDAO();
        lbUsername.setText(clienteLogueado.getNombre() + " " + clienteLogueado.getApellidos());

        initializeHistorialPedidos();
        initializeRealizarPedido();
        initializeEditarPerfil();
    }

    /**
     * El LoginController pasa un objeto del Cliente actual a
     * PrincipalController a través de este método.
     */
    public static void usuarioLogueado(BatoiLogicCliente clienteQueIniciaSesion) {
        clienteLogueado = clienteQueIniciaSesion;
    }

    /**
     * Función que mostrará por pantalla los mensajes de error.
     *
     * @param ex
     */
    private void pantallaErrores(Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Se ha producido un error");
        alert.setContentText(ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("El trazado ha devuelto el siguiente error:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    /**
     * HISTORIAL PEDIDOS.
     */
    @FXML
    private TableView<BatoiLogicPedido> hpTvHistorial;
    @FXML
    private TableColumn<BatoiLogicPedido, Integer> hpClNumPedido;
    @FXML
    private TableColumn<BatoiLogicPedido, Double> hpClPrecio;
    @FXML
    private TableColumn<BatoiLogicPedido, String> hpClFecha;
    @FXML
    private TableColumn<BatoiLogicPedido, Integer> hpClNumProductos;
    @FXML
    private TableColumn<BatoiLogicPedido, String> hpClEstado;
    @FXML
    private DatePicker hpDpDesde;
    @FXML
    private DatePicker hpDpHasta;
    @FXML
    private Button hpBtRestablecer;

    private ArrayList<BatoiLogicPedido> miHistorialPedidos = new ArrayList<>();

    private void initializeHistorialPedidos() {
        try {

            List l = pedidoDAO.findAllId(clienteLogueado.getId());
            BatoiLogicPedido ped = new BatoiLogicPedido();

            for (int i = 0; i < l.size(); i++) {
                ped = (BatoiLogicPedido) l.get(i);
                miHistorialPedidos.add(ped);

            }

            // Configuramos las columnas de la rpTvArticulosDisponibles.
            hpClNumPedido.setCellValueFactory(new PropertyValueFactory<>("id"));
            hpClPrecio.setCellValueFactory(new PropertyValueFactory<>("total"));
            hpClFecha.setCellValueFactory(cellData -> cellData.getValue().fechaProperty());

            hpClNumProductos.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicPedido, Integer> arg0) -> {
                ObservableValue<Integer> obsInt = null;
                try {
                    obsInt = new ReadOnlyObjectWrapper<>(arg0.getValue().getBatoiLogicLinpedidosForPedido().size());
                } catch (Exception ex) {
                    Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
                }

                return obsInt; //To change body of generated lambdas, choose Tools | Templates.
            });

            hpClEstado.setCellValueFactory(new PropertyValueFactory<>("estado"));

            hpTvHistorial.getItems().setAll(miHistorialPedidos);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    void filtrarPedidos() throws ParseException, Exception {
        restablecerHistorial();
        hpBtRestablecer.setDisable(false);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        if (hpDpDesde.getValue() != null && hpDpHasta.getValue() != null) {
            if (hpDpDesde.getValue().compareTo(hpDpHasta.getValue()) == 1) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Seleccion de fecha");
                alert.setContentText("La fecha 'desde' debe ser menor a la fecha 'hasta'");

                alert.showAndWait();
            } else {
                LocalDate desde = hpDpDesde.getValue();
                LocalDate hasta = hpDpHasta.getValue();

                Date fromDate = df.parse(desde.toString());
                Date toDate = df.parse(hasta.toString());
                ArrayList aux = new ArrayList();
                aux.addAll(miHistorialPedidos);
                for (int i = 0; i < aux.size(); i++) {
                    if (!(miHistorialPedidos.get(i).getFecha().after(fromDate) && miHistorialPedidos.get(i).getFecha().before(toDate))) {
                        aux.remove(i);
                    }
                }

                hpTvHistorial.getItems().setAll(aux);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Seleccion de fecha");
            alert.setContentText("Debes seleccionar dos fechas");

            alert.showAndWait();
        }
    }

    @FXML
    void restablecerHistorial() {
        hpTvHistorial.getItems().setAll(miHistorialPedidos);
        hpBtRestablecer.setDisable(true);
    }

    /**
     * CONSULTAR PEDIDO.
     */
    @FXML
    private TextField cpTfPedido;
    @FXML
    private Label cpLbEstado;
    @FXML
    private Label cpLbObservaciones;
    @FXML
    private Label cpLbFecha;
    @FXML
    private TableView<BatoiLogicLinpedido> cpTvProductos;
    @FXML
    private TableColumn<BatoiLogicLinpedido, Integer> cpClId;
    @FXML
    private TableColumn<BatoiLogicLinpedido, String> cpClNombre;
    @FXML
    private TableColumn<BatoiLogicLinpedido, String> cpClDescripcion;
    @FXML
    private TableColumn<BatoiLogicLinpedido, Integer> cpClUnidades;
    @FXML
    private Label cpLbPrecioTotal;

    private BatoiLogicPedido pedido = new BatoiLogicPedido();

    @FXML
    void consultarPedido() {
        try {
            if (cpTfPedido.getText().length() > 0) {
                pedidoDAO = new PedidoDAO();
                pedido = pedidoDAO.findByPKAdvance(Integer.parseInt(cpTfPedido.getText()), clienteLogueado.getId());

                if (pedido != null) {
                    cpLbEstado.setText(pedido.getEstado());
                    cpLbObservaciones.setText(pedido.getObservaciones());
                    cpLbFecha.setText(pedido.getFecha() + "");

                    List<BatoiLogicLinpedido> lineas = lineasPedidoDAO.findBySQL("select a from BatoiLogicLinpedido a where a.batoiLogicPedidoByPedido.id = " + pedido.getId());

                    double precioTotal = 0.0;

                    for (BatoiLogicLinpedido linFact : lineas) {
                        precioTotal += linFact.getBatoiLogicPedidoByProducto().getPrecio() * linFact.getCantidad();
                        cpLbPrecioTotal.setText(df.format(precioTotal) + " €");
                    }

                    cpClId.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicLinpedido, Integer> arg0) -> new ReadOnlyObjectWrapper<>(arg0.getValue().getBatoiLogicPedidoByProducto().getId()));

                    cpClNombre.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicLinpedido, String> arg0) -> new ReadOnlyStringWrapper(String.valueOf(arg0.getValue().getBatoiLogicPedidoByProducto().getNombre())));

                    cpClDescripcion.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicLinpedido, String> arg0) -> new ReadOnlyStringWrapper(String.valueOf(arg0.getValue().getBatoiLogicPedidoByProducto().getDescripcion())));

                    cpClUnidades.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicLinpedido, Integer> arg0) -> new ReadOnlyObjectWrapper<>(arg0.getValue().getCantidad()));

                    cpTvProductos.getItems().setAll(lineas);
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("No se ha encontrado el pedido");
                    alert.setContentText("El número de pedido introducido\n no ha sido encontrado.");

                    alert.showAndWait();
                }
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No se ha encontrado el pedido");
            alert.setContentText("El número de pedido introducido \n no ha sido encontrado para este cliente.");

            alert.showAndWait();
        }
    }

    /**
     * REALIZAR PEDIDO.
     */
    @FXML
    private TableView<BatoiLogicProducto> rpTvArticulosDisponibles;
    @FXML
    private TableColumn<BatoiLogicProducto, Integer> rpClReferencia;
    @FXML
    private TableColumn<BatoiLogicProducto, String> rpClNombre;
    @FXML
    private TableColumn<BatoiLogicProducto, String> rpClDescripcion;
    @FXML
    private TableColumn<BatoiLogicProducto, Double> rpClPrecio;
    @FXML
    private Button rpBtAnyadirACarrito;
    @FXML
    private TableView<BatoiLogicProducto> rpTvCarrito;
    @FXML
    private TableColumn<BatoiLogicProducto, Integer> rpClMiReferencia;
    @FXML
    private TableColumn<BatoiLogicProducto, String> rpClMiNombre;
    @FXML
    private TableColumn<BatoiLogicProducto, String> rpClMiDescripcion;
    @FXML
    private TableColumn<BatoiLogicProducto, Double> rpClMiPrecio;
    @FXML
    private TableColumn<BatoiLogicProducto, Integer> rpClMiCantidad;
    @FXML
    private Label rpLbPrecioTotal;
    @FXML
    private Button rpBtRealizarPedido;
    @FXML
    private Button rpBtVaciarCarrito;
    @FXML
    private Button rpBtEliminarLinea;
    @FXML
    private Button rpBtEditarCantidad;

    private void initializeRealizarPedido() {

        try {
            productosDisponibles = productosDAO.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Configuramos las columnas de la rpTvArticulosDisponibles.
        rpClReferencia.setCellValueFactory(new PropertyValueFactory<>("id"));
        rpClNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        rpClDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        rpClPrecio.setCellValueFactory(new PropertyValueFactory<>("precio"));

        rpTvArticulosDisponibles.getItems().setAll(productosDisponibles);
        productosDisponibles.clear();

        // Configuramos las columnas de la rpTvCarrito.
    }

    private List<BatoiLogicProducto> productosDisponibles = new ArrayList<>();
    private List<BatoiLogicProducto> productosEnCarrito = new ArrayList<>();

    /**
     * Funcion que detecta si un registro ha sido seleccionado.
     */
    public void registroSeleccionadoEnArticulosDisp() {

        if (rpTvArticulosDisponibles.getSelectionModel().getSelectedItem() != null) {
            rpBtAnyadirACarrito.setDisable(false);
        } else {
            rpBtAnyadirACarrito.setDisable(true);
        }

    }

    /**
     * Funcion que detecta si un registro ha sido seleccionado.
     */
    public void registroSeleccionadoEnCarrito() {

        if (rpTvCarrito.getSelectionModel().getSelectedItem() != null) {
            rpBtEditarCantidad.setDisable(false);
            rpBtEliminarLinea.setDisable(false);
        } else {
            rpBtEditarCantidad.setDisable(true);
            rpBtEliminarLinea.setDisable(true);
        }

    }

    @FXML
    void anyadirCarrito() {
        BatoiLogicProducto productoSeleccionado = new BatoiLogicProducto(rpTvArticulosDisponibles.getSelectionModel().getSelectedItem());

        TextInputDialog dialog = new TextInputDialog("1");
        dialog.setTitle("Confirmar cantidad");
        dialog.setHeaderText("Por favor, indique la cantidad a comprar");
        dialog.setContentText("Introduzca un valor numérico entero:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                if (Integer.parseInt(result.get()) > 0) {
                    productoSeleccionado.setStock(Integer.parseInt(result.get()));

                    // Si el producto a comprar ya está en carrito, editamos su stock en carrito.
                    boolean productoEnCarrito = false;
                    for (int i = 0; i < productosEnCarrito.size() && !productoEnCarrito; i++) {
                        if (productoSeleccionado.getId() == productosEnCarrito.get(i).getId()) {

                            productosEnCarrito.get(i).setStock(productoSeleccionado.getStock() + productosEnCarrito.get(i).getStock());
                            productoEnCarrito = true;
                        }
                    }

                    // Si no, lo guardamos en el carrito.
                    if (!productoEnCarrito) {
                        productosEnCarrito.add(productoSeleccionado);
                    }

                    rpClMiReferencia.setCellValueFactory(new PropertyValueFactory<>("id"));
                    rpClMiNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
                    rpClMiDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
                    rpClMiPrecio.setCellValueFactory(cellData -> {
                        ObservableValue<Double> obsInt = new ReadOnlyObjectWrapper<>(cellData.getValue().getPrecio());
                        return obsInt;
                    });
                    rpClMiCantidad.setCellValueFactory(new PropertyValueFactory<>("stock"));

                    rpTvCarrito.getItems().setAll(productosEnCarrito);

                    rpBtRealizarPedido.setDisable(false);
                    rpBtVaciarCarrito.setDisable(false);

                    // Mostramos el total del carrito.
                    double totalCarrito = 0.00;
                    for (BatoiLogicProducto producto : productosEnCarrito) {
                        totalCarrito += producto.getPrecio() * producto.getStock();
                    }
                    rpLbPrecioTotal.setText(df.format(totalCarrito) + " €");
                } else {
                    throw new Exception("No se puede introducir 0 o menos");
                }

            } catch (Exception e) {
                //pantallaErrores(new Exception("No se ha introducido un valor numérico válido. No se añadirá el producto al carrito."));
                e.printStackTrace();
            }
        }

    }

    @FXML
    void vaciarCarrito() {
        productosEnCarrito.clear();
        rpTvCarrito.getItems().setAll(productosEnCarrito);
        rpLbPrecioTotal.setText("0.00 €");

        rpBtRealizarPedido.setDisable(true);
        rpBtEditarCantidad.setDisable(true);
        rpBtEliminarLinea.setDisable(true);
        rpBtVaciarCarrito.setDisable(true);
    }

    @FXML
    void eliminarLineaCarrito() {
        BatoiLogicProducto lineaSeleccionada = rpTvCarrito.getSelectionModel().getSelectedItem();

        productosEnCarrito.remove(lineaSeleccionada);
        rpTvCarrito.getItems().setAll(productosEnCarrito);
        rpBtEliminarLinea.setDisable(true);
        rpBtEditarCantidad.setDisable(true);

        if (productosEnCarrito.size() == 0) {
            vaciarCarrito();
        } else {
            // Actualizamos el total del carrito.
            double totalCarrito = 0.00;
            for (BatoiLogicProducto producto : productosEnCarrito) {
                totalCarrito += producto.getPrecio() * producto.getStock();
            }
            rpLbPrecioTotal.setText(df.format(totalCarrito) + " €");
        }
    }

    @FXML
    void editarCantidadCarrito() {
        if (rpTvCarrito.getSelectionModel().getSelectedItem() != null) {

            TextInputDialog dialog = new TextInputDialog("" + rpTvCarrito.getSelectionModel().getSelectedItem().getStock());
            dialog.setTitle("Editar cantidad");
            dialog.setHeaderText("Por favor, indique la nueva cantidad a comprar");
            dialog.setContentText("Introduzca un valor numérico entero:");

            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                try {

                    if (Integer.parseInt(result.get()) > 0) {
                        rpTvCarrito.getSelectionModel().getSelectedItem().setStock(Integer.parseInt(result.get()));
                        rpTvCarrito.getItems().setAll(productosEnCarrito);

                        // Actualizamos el total del carrito.
                        double totalCarrito = 0.00;
                        for (BatoiLogicProducto producto : productosEnCarrito) {
                            totalCarrito += producto.getPrecio() * producto.getStock();
                        }
                        rpLbPrecioTotal.setText(df.format(totalCarrito) + " €");

                        rpBtRealizarPedido.setDisable(false);
                        rpBtVaciarCarrito.setDisable(false);
                    } else {
                        throw new Exception("No se puede introducir 0 o menos");
                    }

                } catch (Exception e) {
                    pantallaErrores(new Exception("No se ha introducido un valor numérico válido. No se modificará el producto del carrito."));
                }
            }

        }
    }

    @FXML
    void realizarPedido(ActionEvent event) {
        try {
            ConfirmarPedidoController.establecerPedido(productosEnCarrito, clienteLogueado);
            rootPane.getChildren().setAll(loadFXML("ViewConfirmarPedido"));
        } catch (IOException e) {
            pantallaErrores(e);
        }
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * EDITAR PERFIL.
     */
    @FXML
    private TextField epTfNombre;
    @FXML
    private TextField epTfApellidos;
    @FXML
    private TextField epTfEmail;
    @FXML
    private PasswordField epPfContrasenya;
    @FXML
    private PasswordField epPfConfContrasenya;
    @FXML
    private TableView<BatoiLogicDireccion> epTvDirecciones;
    @FXML
    private TableColumn<BatoiLogicDireccion, String> epClCalle;
    @FXML
    private TableColumn<BatoiLogicDireccion, String> epClCiudad;
    @FXML
    private TableColumn<BatoiLogicDireccion, String> epClCP;
    @FXML
    private TableColumn<BatoiLogicDireccion, String> epClProvincia;
    @FXML
    private TableColumn<BatoiLogicDireccion, String> epClPais;
    @FXML
    private Button epBtEditarDireccion;
    @FXML
    private Button epBtBorrarDireccion;

    private List<BatoiLogicDireccion> misDirecciones = new ArrayList<>();

    private void initializeEditarPerfil() {
        try {
            // Configuramos las columnas de la epTvDirecciones.
            TableColumn<BatoiLogicDireccion, String> ciudad = new TableColumn<>("Ciudad");

            epClCalle.setCellValueFactory(new PropertyValueFactory<>("calle"));

            epClCP.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicDireccion, String> arg0)
                    -> new ReadOnlyStringWrapper(String.valueOf(arg0.getValue().getBatoiLogicCp().getCp())));
            epClProvincia.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicDireccion, String> arg0)
                    -> new ReadOnlyStringWrapper(String.valueOf(arg0.getValue().getBatoiLogicCp().getBatoiLogicCiudad().getProvincia())));
            epClPais.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicDireccion, String> arg0)
                    -> new ReadOnlyStringWrapper(String.valueOf(arg0.getValue().getBatoiLogicCp().getBatoiLogicCiudad().getPais())));
            epClCiudad.setCellValueFactory((TableColumn.CellDataFeatures<BatoiLogicDireccion, String> arg0)
                    -> new ReadOnlyStringWrapper(String.valueOf(arg0.getValue().getBatoiLogicCp().getBatoiLogicCiudad().getNombre())));

            misDirecciones = direccionDAO.findAllId(clienteLogueado.getId());

            epTvDirecciones.getItems().setAll(misDirecciones);

            epTfEmail.setText(clienteLogueado.getEmail());
            epTfNombre.setText(clienteLogueado.getNombre());
            epTfApellidos.setText(clienteLogueado.getApellidos());

        } catch (Exception ex) {
            Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void actualizarInformacion() throws Exception {

        if (!epTfNombre.getText().trim().equals("")
                && !epTfApellidos.getText().trim().equals("")
                && !epTfEmail.getText().trim().equals("")) {//To change body of generated lambdas, choose Tools | Templates.
            if (epPfContrasenya.getText().length() > 0) {

                if (!epPfConfContrasenya.getText().equals(epPfContrasenya.getText())) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Ventana de ERROR");
                    alert.setHeaderText(null);
                    alert.setContentText("No se ha podido cambiar la contraseña, ya que no coinciden. Inténtelo de nuevo.");

                    alert.showAndWait();
                } else {
                    clienteLogueado.setApellidos(epTfApellidos.getText());
                    clienteLogueado.setNombre(epTfNombre.getText());
                    clienteLogueado.setEmail(epTfEmail.getText());
                    clienteLogueado.setPassw(epPfContrasenya.getText());

                }
            } else {
                clienteLogueado.setApellidos(epTfApellidos.getText());
                clienteLogueado.setNombre(epTfNombre.getText());
                clienteLogueado.setEmail(epTfEmail.getText());
            }
            clienteDAO.update(clienteLogueado);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ventana de confirmación");
            alert.setHeaderText(null);
            alert.setContentText("La información de perfil ha sido actualizada correctamente.");

            alert.showAndWait();

            epPfContrasenya.setText("");
            epPfConfContrasenya.setText("");
            epPfConfContrasenya.setDisable(true);

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ventana de ERROR");
            alert.setHeaderText(null);
            alert.setContentText("No se ha podido realizar la actualización del perfil. Inténtelo de nuevo.");

            alert.showAndWait();
        }
    }

    @FXML
    boolean comprobarContrasenya() {
        boolean estado;

        if (epPfContrasenya.getText().length() > 0) {
            epPfConfContrasenya.setDisable(false);
            estado = epPfConfContrasenya.getText().equals(epPfContrasenya.getText());
        } else {
            epPfConfContrasenya.setDisable(true);
            epPfConfContrasenya.setText("");
            estado = true;
        }

        return estado;
    }

    /**
     * Funcion que detecta si un registro ha sido seleccionado.
     */
    public void registroSeleccionadoEnDirecciones() {

        if (epTvDirecciones.getSelectionModel().getSelectedItem() != null) {
            epBtEditarDireccion.setDisable(false);
            epBtBorrarDireccion.setDisable(false);
        } else {
            epBtEditarDireccion.setDisable(true);
            epBtBorrarDireccion.setDisable(true);
        }

    }

    @FXML
    void eliminarDireccion() throws Exception {
        BatoiLogicDireccion lineaSeleccionada = epTvDirecciones.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Cuadro de confimación");
        alert.setHeaderText("¿Desea eliminar esta dirección?");
        alert.setContentText(lineaSeleccionada.toString());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            misDirecciones.remove(lineaSeleccionada);
            direccionDAO.delete(lineaSeleccionada);
            epTvDirecciones.getItems().setAll(misDirecciones);
            epBtEditarDireccion.setDisable(true);
            epBtBorrarDireccion.setDisable(true);
        }
    }

    @FXML
    void editarDireccion() {
        try {
            EditarDireccionController.establecerDireccion(epTvDirecciones.getSelectionModel().getSelectedItem(), null);
            rootPane.getChildren().setAll(loadFXML("ViewEditarDireccion"));
        } catch (IOException e) {
            pantallaErrores(e);
        }
    }

    @FXML
    void anyadirDireccion() {
        try {
            EditarDireccionController.establecerDireccion(null, clienteLogueado);
            rootPane.getChildren().setAll(loadFXML("ViewEditarDireccion"));
        } catch (IOException e) {
            pantallaErrores(e);
        }
    }

}
