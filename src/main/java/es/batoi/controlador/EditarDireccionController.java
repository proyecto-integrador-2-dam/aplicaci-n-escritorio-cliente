package es.batoi.controlador;

import es.batoi.App;
import es.batoi.Dao.CodigoPostalDAO;
import es.batoi.Dao.DireccionDAO;
import es.batoi.modelos.BatoiLogicCliente;
import es.batoi.modelos.BatoiLogicCp;
import es.batoi.modelos.BatoiLogicDireccion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EditarDireccionController implements Initializable {

    @FXML
    private TextField tfCalle;

    @FXML
    private ComboBox tfCP;

    @FXML
    private TextField tfProvincia;

    @FXML
    private TextField tfCiudad;

    private static BatoiLogicCliente clienteLogueado;

    private static BatoiLogicDireccion direccion;
    private List <BatoiLogicCp> l;
    CodigoPostalDAO postalDAO;
    DireccionDAO direcDAO;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            postalDAO = new CodigoPostalDAO();
            direcDAO = new DireccionDAO();

            if (direccion != null) {
                tfCalle.setText(direccion.getCalle());
                tfCP.setValue(direccion.getBatoiLogicCp());
                tfProvincia.setText(direccion.getBatoiLogicCp().getBatoiLogicCiudad().getProvincia());
                tfCiudad.setText(direccion.getBatoiLogicCp().getBatoiLogicCiudad().getNombre());
            }

            l = postalDAO.findAll();

            ObservableList list = FXCollections.observableArrayList(l);
            tfCP.getItems().setAll(list);

        } catch (Exception ex) {
            Logger.getLogger(EditarDireccionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que llamamos desde PrincipalController para obtener la dirección a
     * editar.
     */
    public static void establecerDireccion(BatoiLogicDireccion direccionAEditar, BatoiLogicCliente clienteActual) {
        direccion = direccionAEditar;
        if (clienteActual != null) {
            clienteLogueado = clienteActual;
        }
    }

    @FXML
    void cerrarVentana(ActionEvent event) {
        try {
            Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            primaryStage.setScene(new Scene(loadFXML("ViewPrincipal")));
            primaryStage.setTitle("BatoiLOGIC");
            primaryStage.setResizable(false);
            primaryStage.centerOnScreen();
            primaryStage.show();
        } catch (IOException e) {
            pantallaErrores(e);
        }
    }

    @FXML
    void guardarDireccion(ActionEvent event) {
        try {
            if (tfCalle.getText().trim().length() > 0
                    && tfCP.getSelectionModel().getSelectedItem() != null) {

                if (direccion == null) {
                    direccion = new BatoiLogicDireccion();
                    direccion.setCalle(tfCalle.getText());
                    direccion.setBatoiLogicCliente(clienteLogueado);

                    BatoiLogicCp aux=(BatoiLogicCp) tfCP.getSelectionModel().getSelectedItem();
                    BatoiLogicCp cp=postalDAO.findByPK(aux.getId());

                    direccion.setBatoiLogicCp(cp);
                    direccion.getBatoiLogicCp().setCp(cp.getCp());

                    direcDAO.insert(direccion);
                }

                else {
                    direccion.setCalle(tfCalle.getText());

                    BatoiLogicCp aux=(BatoiLogicCp) tfCP.getSelectionModel().getSelectedItem();
                    BatoiLogicCp cp=postalDAO.findByPK(aux.getId());

                    direccion.setBatoiLogicCp(cp);
                    direccion.getBatoiLogicCp().setCp(cp.getCp());

                    direcDAO.update(direccion);
                }

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Cuadro de información");
                alert.setHeaderText(null);
                alert.setContentText("La dirección ha sido actualizada correctamente.");

                alert.showAndWait();

                cerrarVentana(event);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Cuadro de ERROR");
                alert.setHeaderText(null);
                alert.setContentText("No se ha podido actualizar la dirección. Complete todos los campos.");

                alert.showAndWait();
            }
        } catch (Exception e) {
            e.printStackTrace();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Cuadro de ERROR");
            alert.setHeaderText(null);
            alert.setContentText("No se ha podido actualizar la dirección. Complete todos los campos.");
            alert.showAndWait();

            e.printStackTrace();
        }
    }

    /**
     * Función que mostrará por pantalla los mensajes de error.
     *
     * @param ex
     */
    private void pantallaErrores(Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Se ha producido un error");
        alert.setContentText(ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("El trazado ha devuelto el siguiente error:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }
}
