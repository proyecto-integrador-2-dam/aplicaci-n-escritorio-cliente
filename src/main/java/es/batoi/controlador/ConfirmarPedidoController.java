package es.batoi.controlador;

import es.batoi.App;
import es.batoi.Dao.DireccionDAO;
import es.batoi.Dao.LineasPedidoDAO;
import es.batoi.Dao.PedidoDAO;
import es.batoi.modelos.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ConfirmarPedidoController implements Initializable {

    @FXML
    private Label lbPrecioTotal;
    @FXML
    private Button btRealizarPedido;
    @FXML
    private TableView<BatoiLogicProducto> rpTvCarrito;
    @FXML
    private TableColumn<BatoiLogicProducto, Integer> clMiReferencia;
    @FXML
    private TableColumn<BatoiLogicProducto, String> clMiNombre;
    @FXML
    private TableColumn<BatoiLogicProducto, String> clMiDescripcion;
    @FXML
    private TableColumn<BatoiLogicProducto, Double> clMiPrecio;
    @FXML
    private TableColumn<BatoiLogicProducto, Integer> clMiCantidad;
    @FXML
    private TextField tfCalle;
    @FXML
    private TextField tfCP;
    @FXML
    private ComboBox<BatoiLogicDireccion> cbDirecciones;
    @FXML
    private TextField tfProvincia;
    @FXML
    private TextField tfCiudad;
    @FXML
    private Label lbUsuario;

    private DireccionDAO direccionDAO;
    private PedidoDAO pedidoDAO;
    private LineasPedidoDAO lineasPedidoDAO;

    private static BatoiLogicCliente clienteLogueado;

    private List<BatoiLogicDireccion> direcciones = new ArrayList<>();
    private static List<BatoiLogicProducto> productos = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        lbUsuario.setText(clienteLogueado.getNombre() + " " + clienteLogueado.getApellidos());

        try {
            direccionDAO = new DireccionDAO();
            direcciones = direccionDAO.findAllId(clienteLogueado.getId());
            ObservableList<BatoiLogicDireccion> l = FXCollections.observableArrayList(direcciones);
            cbDirecciones.setItems(l);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Configuramos las columnas.
        clMiReferencia.setCellValueFactory(new PropertyValueFactory<>("id"));
        clMiNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        clMiDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        clMiPrecio.setCellValueFactory(new PropertyValueFactory<>("precio"));
        clMiCantidad.setCellValueFactory(new PropertyValueFactory<>("stock"));

        rpTvCarrito.getItems().setAll(productos);

        // Mostramos el total del carrito.
        double totalCarrito = 0.00;
        for (BatoiLogicProducto producto : productos) {
            totalCarrito += producto.getPrecio() * producto.getStock();
        }

        lbPrecioTotal.setText(((double)Math.round(totalCarrito * 100d) / 100d) + " €");

        cbDirecciones.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> comprobarDireccion());
    }

    /**
     * Método que llamamos desde PrincipalController para obtener los productos
     * seleccionados.
     */
    public static void establecerPedido(List<BatoiLogicProducto> productosSeleccionados, BatoiLogicCliente clienteActual) {
        productos = productosSeleccionados;
        clienteLogueado = clienteActual;
    }

    @FXML
    void realizarPedido(ActionEvent event) throws Exception {

        pedidoDAO = new PedidoDAO();
        lineasPedidoDAO = new LineasPedidoDAO();

        BatoiLogicPedido pedido = new BatoiLogicPedido();
        pedido.setEstado("EN PREPARACION");
        pedido.setObservaciones("Su pedido ha sido recibido con exito");
        pedido.setTotal(Double.valueOf(lbPrecioTotal.getText().split(" ")[0]));
        pedido.setFecha(new Date());
        pedido.setBatoiLogicCliente(clienteLogueado);
        pedido.setBatoiLogicDireccion(cbDirecciones.getSelectionModel().getSelectedItem());

        pedidoDAO.insert(pedido);

        for (int i = 0; i < productos.size(); i++) {
            BatoiLogicLinpedido lineas = new BatoiLogicLinpedido();
            lineas.setCantidad(productos.get(i).getStock());
            lineas.setLinea(i + 1);
            lineas.setBatoiLogicPedidoByPedido(pedido);
            lineas.setBatoiLogicPedidoByProducto(productos.get(i));
            lineasPedidoDAO.insert(lineas);
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Pedido realizado");
        alert.setHeaderText("Gracias por su compra");
        alert.setContentText("El pedido ha sido realizado correctamente, en breve recibirá actualizaciones sobre su estado.");

        alert.showAndWait();

        cerrarVentana(event);
    }

    @FXML
    void comprobarDireccion() {
        BatoiLogicDireccion direccionSeleccionada = cbDirecciones.getSelectionModel().getSelectedItem();
        if (direccionSeleccionada != null) {
            tfCiudad.setText(direccionSeleccionada.getBatoiLogicCp().getBatoiLogicCiudad().getNombre());
            tfProvincia.setText(direccionSeleccionada.getBatoiLogicCp().getBatoiLogicCiudad().getProvincia());
            tfCP.setText(String.valueOf(direccionSeleccionada.getBatoiLogicCp().getCp()));
            tfCalle.setText(direccionSeleccionada.getCalle());
            btRealizarPedido.setDisable(false);
        }
    }

    @FXML
    void cerrarVentana(ActionEvent event) {
        try {
            Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            primaryStage.setScene(new Scene(loadFXML("ViewPrincipal")));
            primaryStage.setTitle("BatoiLOGIC");
            primaryStage.setResizable(false);
            primaryStage.centerOnScreen();
            primaryStage.show();
        } catch (IOException e) {
            pantallaErrores(e);
        }
    }

    /**
     * Función que mostrará por pantalla los mensajes de error.
     *
     * @param ex
     */
    private void pantallaErrores(Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Se ha producido un error");
        alert.setContentText(ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("El trazado ha devuelto el siguiente error:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

}
