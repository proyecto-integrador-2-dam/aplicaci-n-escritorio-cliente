/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.controlador;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import es.batoi.App;
import es.batoi.Dao.ClienteDAO;
import es.batoi.modelos.BatoiLogicCliente;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author batoi
 */
public class RegisterController implements Initializable {

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfDni;

    @FXML
    private TextField tfApellidos;

    @FXML
    private TextField tfMail;

    @FXML
    private TextField tfContrasenya;

    @FXML
    private TextField tfConfContrasenya;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

    }

    @FXML
    private void registrarse(ActionEvent event) {
        if (tfNombre.getText().trim().length() > 0
                && tfDni.getText().trim().length() > 0
                && tfApellidos.getText().trim().length() > 0
                && tfMail.getText().trim().length() > 0
                && tfContrasenya.getText().trim().length() > 0
                && tfConfContrasenya.getText().trim().length() > 0
                && tfContrasenya.getText().equals(tfConfContrasenya.getText())) {

            try {
                ClienteDAO clienteDAO = new ClienteDAO();

                clienteDAO.insert(new BatoiLogicCliente(tfDni.getText(), tfNombre.getText(), tfContrasenya.getText(), tfApellidos.getText(), tfMail.getText()));

                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Usuario creado");
                alert.setHeaderText(null);
                alert.setContentText("El usuario ha sido registrado correctamente. Pruebe a iniciar sesión.");

                alert.showAndWait();

                Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                // Cerramos la ventana.
                primaryStage.close();
                clienteDAO.cerrar();
                // Reiniciamos el programa (para volver a mostrar el inicio de sesión).
                Platform.runLater(() -> {
                    try {
                        new App().start(new Stage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Error");
            alert.setHeaderText("Revise los datos introducidos");
            alert.setContentText("Debe completar todos los campos y asegurarse de que las contraseñas coincidan.");

            alert.showAndWait();
        }
    }

}
