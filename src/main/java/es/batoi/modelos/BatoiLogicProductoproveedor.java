package es.batoi.modelos;
// Generated 14 feb. 2020 20:52:32 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * BatoiLogicProductoproveedor generated by hbm2java
 */
public class BatoiLogicProductoproveedor  implements java.io.Serializable {


     private int id;
     private BatoiLogicPedido batoiLogicPedido;
     private BatoiLogicProveedor batoiLogicProveedor;
     private ResUsers resUsersByCreateUid;
     private ResUsers resUsersByWriteUid;
     private double precio;
     private Date createDate;
     private Date writeDate;

    public BatoiLogicProductoproveedor() {
    }

	
    public BatoiLogicProductoproveedor(int id, double precio) {
        this.id = id;
        this.precio = precio;
    }
    public BatoiLogicProductoproveedor(int id, BatoiLogicPedido batoiLogicPedido, BatoiLogicProveedor batoiLogicProveedor, ResUsers resUsersByCreateUid, ResUsers resUsersByWriteUid, double precio, Date createDate, Date writeDate) {
       this.id = id;
       this.batoiLogicPedido = batoiLogicPedido;
       this.batoiLogicProveedor = batoiLogicProveedor;
       this.resUsersByCreateUid = resUsersByCreateUid;
       this.resUsersByWriteUid = resUsersByWriteUid;
       this.precio = precio;
       this.createDate = createDate;
       this.writeDate = writeDate;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public BatoiLogicPedido getBatoiLogicPedido() {
        return this.batoiLogicPedido;
    }
    
    public void setBatoiLogicPedido(BatoiLogicPedido batoiLogicPedido) {
        this.batoiLogicPedido = batoiLogicPedido;
    }
    public BatoiLogicProveedor getBatoiLogicProveedor() {
        return this.batoiLogicProveedor;
    }
    
    public void setBatoiLogicProveedor(BatoiLogicProveedor batoiLogicProveedor) {
        this.batoiLogicProveedor = batoiLogicProveedor;
    }
    public ResUsers getResUsersByCreateUid() {
        return this.resUsersByCreateUid;
    }
    
    public void setResUsersByCreateUid(ResUsers resUsersByCreateUid) {
        this.resUsersByCreateUid = resUsersByCreateUid;
    }
    public ResUsers getResUsersByWriteUid() {
        return this.resUsersByWriteUid;
    }
    
    public void setResUsersByWriteUid(ResUsers resUsersByWriteUid) {
        this.resUsersByWriteUid = resUsersByWriteUid;
    }
    public double getPrecio() {
        return this.precio;
    }
    
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public Date getWriteDate() {
        return this.writeDate;
    }
    
    public void setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
    }




}


