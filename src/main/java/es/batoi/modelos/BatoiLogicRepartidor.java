package es.batoi.modelos;
// Generated 14 feb. 2020 20:52:32 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * BatoiLogicRepartidor generated by hbm2java
 */
public class BatoiLogicRepartidor  implements java.io.Serializable {


     private int id;
     private BatoiLogicCamion batoiLogicCamion;
     private ResUsers resUsersByWriteUid;
     private ResUsers resUsersByCreateUid;
     private String nombre;
     private String passw;
     private String apellidos;
     private Date createDate;
     private Date writeDate;
     private Set batoiLogicRutas = new HashSet(0);
     private Set batoiLogicUbicacions = new HashSet(0);

    public BatoiLogicRepartidor() {
    }

	
    public BatoiLogicRepartidor(int id, BatoiLogicCamion batoiLogicCamion, String nombre, String passw, String apellidos) {
        this.id = id;
        this.batoiLogicCamion = batoiLogicCamion;
        this.nombre = nombre;
        this.passw = passw;
        this.apellidos = apellidos;
    }
    public BatoiLogicRepartidor(int id, BatoiLogicCamion batoiLogicCamion, ResUsers resUsersByWriteUid, ResUsers resUsersByCreateUid, String nombre, String passw, String apellidos, Date createDate, Date writeDate, Set batoiLogicRutas, Set batoiLogicUbicacions) {
       this.id = id;
       this.batoiLogicCamion = batoiLogicCamion;
       this.resUsersByWriteUid = resUsersByWriteUid;
       this.resUsersByCreateUid = resUsersByCreateUid;
       this.nombre = nombre;
       this.passw = passw;
       this.apellidos = apellidos;
       this.createDate = createDate;
       this.writeDate = writeDate;
       this.batoiLogicRutas = batoiLogicRutas;
       this.batoiLogicUbicacions = batoiLogicUbicacions;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public BatoiLogicCamion getBatoiLogicCamion() {
        return this.batoiLogicCamion;
    }
    
    public void setBatoiLogicCamion(BatoiLogicCamion batoiLogicCamion) {
        this.batoiLogicCamion = batoiLogicCamion;
    }
    public ResUsers getResUsersByWriteUid() {
        return this.resUsersByWriteUid;
    }
    
    public void setResUsersByWriteUid(ResUsers resUsersByWriteUid) {
        this.resUsersByWriteUid = resUsersByWriteUid;
    }
    public ResUsers getResUsersByCreateUid() {
        return this.resUsersByCreateUid;
    }
    
    public void setResUsersByCreateUid(ResUsers resUsersByCreateUid) {
        this.resUsersByCreateUid = resUsersByCreateUid;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getPassw() {
        return this.passw;
    }
    
    public void setPassw(String passw) {
        this.passw = passw;
    }
    public String getApellidos() {
        return this.apellidos;
    }
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public Date getWriteDate() {
        return this.writeDate;
    }
    
    public void setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
    }
    public Set getBatoiLogicRutas() {
        return this.batoiLogicRutas;
    }
    
    public void setBatoiLogicRutas(Set batoiLogicRutas) {
        this.batoiLogicRutas = batoiLogicRutas;
    }
    public Set getBatoiLogicUbicacions() {
        return this.batoiLogicUbicacions;
    }
    
    public void setBatoiLogicUbicacions(Set batoiLogicUbicacions) {
        this.batoiLogicUbicacions = batoiLogicUbicacions;
    }




}


